<?php
/**
 * Created by PhpStorm.
 * User: yyq
 * Date: 2018/8/20
 * Time: 21:57
 */

namespace Member\Controller;


use Common\Controller\AdminbaseController;
use Common\Model\FutureModel;

class FutureController extends AdminbaseController
{
    private $future_model;

    public function __construct()
    {
        parent::__construct();
        $this->future_model = new FutureModel();
    }

    public function lists() {
        $where = [];
        if(IS_POST) {
            $phone = I('post.phone');
            if($phone) {
                $where['b.phone'] = $phone;
            }

        } else {
            $phone = I('get.phone');
            if($phone) {
                $where['b.phone'] = $phone;
            }

        }
        $count = $this->future_model
            ->alias('a')
            ->join('left join sw_member as b on a.openid = b.openid')
            ->where($where)
            ->count();

        $page=  $this->page($count, C("PAGE_NUMBER"));

        $result = $this->future_model
            ->alias('a')
            ->join('left join sw_member as b on a.openid = b.openid')
            ->where($where)
            ->limit($page->firstRow, $page->listRows)
            ->field('a.id, a.times, a.period, a.create_time, b.phone, b.openid, child_name, birthday')
            ->order('a.times desc, a.create_time desc')
            ->select();

        $para['list'] = $result;
        $para['Page'] = $page->show();
        $para['formget']    = I('');
        $this->assign($para);
        $this->display();
    }

}