<?php
/**
 * Created by PhpStorm.
 * User: yyq
 * Date: 2018/7/4
 * Time: 23:12
 */

namespace Member\Controller;


use Common\Controller\AdminbaseController;
use Common\Model\CardModel;

class CardController extends AdminbaseController
{
    private $card_model;

    public function __construct()
    {
        parent::__construct();
        $this->card_model   = new CardModel();
    }

    public function lists() {
        $count = $this->card_model->count();

        $page = $this->page($count, C("PAGE_NUMBER"));

        $result = $this->card_model
            ->limit($page->firstRow, $page->listRows)
            ->select();

        $para['list'] = $result;
        $para['Page'] = $page->show();
        $this->assign($para);
        $this->display();
    }

    public function add() {
        if(IS_POST) {
//            dump($_POST);exit;
            $post = I('post.post');
            $post['create_time'] = date('Y-m-d H:i:s');
            $post['update_time'] = date('Y-m-d H:i:s');

            if($this->card_model->add($post) == false) {
                $this->error('添加失败');
            }
            $this->success('', U('lists'));
        } else {

            $this->assign('options', $this->card_model->getTypeOption());
            $this->display();
        }
    }

    public function edit() {
        $id = I('get.id');
        if(IS_POST) {
            $post = I('post.post');//dump($post);die;
            $post['update_time'] = date('Y-m-d H:i:s');

            if($this->card_model->save($post) == false) {
                $this->error('编辑失败');
            }
            $this->success('', U('lists'));
        } else {
            $info = $this->card_model->find($id);
            $this->assign($info);//dump($info);
            $this->assign('options', $this->card_model->getTypeOption($info['type']));
            $this->display();
        }
    }

    public function del() {
        $id = I('get.id');
        if($this->card_model->delete($id) == false) {
            $this->error('操作失败');
        }
        $this->success('');
    }
}