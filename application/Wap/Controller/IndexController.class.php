<?php
namespace Wap\Controller;


use System\Model\DocumentsModel;

class IndexController extends WapBaseController
{
    private $document_model;

    public function __construct()
    {
        parent::__construct();
        $this->document_model   = new DocumentsModel();
    }

    public function index()
    {
        $info = $this->document_model->where(['doc_class' => 'about'])->find();

        $this->assign('content', $info['content']);
        $this->display();
    }
}