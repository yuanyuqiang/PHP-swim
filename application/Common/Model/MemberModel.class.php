<?php
/**
 * Created by PhpStorm.
 * User: yyq
 * Date: 2018/7/4
 * Time: 23:09
 */

namespace Common\Model;


class MemberModel extends CommonModel
{
    /**
     * @param $openid
     * @param $nickname
     * @return array|bool
     */
    public function register($openid, $nickname) {
        $data = [
            'openid'    => $openid,
            'nickname'  => $nickname,
            'phone'     => '',
            'create_time'   => date('Y-m-d H:i:s'),
            'update_time'   => date('Y-m-d H:i:s'),
        ];
        $id = $this->add($data);
        if($id) {
            $data['id'] = $id;
            return $data;
        } else {
            return false;
        }
    }

    /**
     * @param $openid
     * @param $phone
     * @return bool
     */
    public function savePhone($openid, $phone) {
        $data = [
            'phone'         => $phone,
            'update_time'   => date('Y-m-d H:i:s'),
        ];
        return $this->where(['openid' => $openid])->save($data);
    }
}