<?php
/**
 * Created by PhpStorm.
 * User: yyq
 * Date: 2018/7/15
 * Time: 16:10
 */

namespace Common\Model;


class RecordModel extends CommonModel
{
    const TYPE_ADMIN= 'admin';//后台打卡
    const TYPE_USER = 'user';//用户打卡

    /**
     * @param $card_id
     * @param $openid
     * @param $type
     * @return mixed
     */
    public function addOne($card_id, $openid = '', $type, $tag, $phone) {
        return $this->add(
            [
                'openid'    => $openid,
                'card_id'   => $card_id,
                'create_time'   => date('Y-m-d H:i:s'),
                'type'      => $type,
                'tag'      => $tag,
                'phone'     => $phone,
            ]
        );
    }
}