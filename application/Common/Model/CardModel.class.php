<?php
/**
 * Created by PhpStorm.
 * User: yyq
 * Date: 2018/7/4
 * Time: 23:10
 */

namespace Common\Model;


class CardModel extends CommonModel
{
    const TYPE_TIMES  = 'times';//次数卡
    const TYPE_PERIOD = 'period';//时长卡

    public $type = [
        'times'     => '次数卡',
        'period'    => '时长卡'
    ];

    /**
     * @param string $type
     * @return string
     */
    public function getTypeOption($type= '') {
        foreach ($this->type as $k => $v) {
            if($type == $k) {
                $options .= '<option value="'.$k.'" selected>'.$v.'</option>';
            } else {
                $options .= '<option value="'.$k.'">'.$v.'</option>';
            }
        }
        return $options;
    }


}