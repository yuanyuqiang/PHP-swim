<?php
/**
 * Created by PhpStorm.
 * User: yyq
 * Date: 2018/7/22
 * Time: 14:39
 */

namespace Common\Model;


class OrderModel extends CommonModel
{

    /**
     * 生成订单编号
     * @return string
     */
    public function getOrderNumber()
    {
        $num = 'D'.date('Ymd') . str_pad(mt_rand(1, 99999), 5, '0', STR_PAD_LEFT);
        if($this->where(['order_sn' => $num])->find()) {
            self::getOrderNumber();
        }
        return $num;
    }
}