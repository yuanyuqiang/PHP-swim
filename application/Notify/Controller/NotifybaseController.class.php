<?php
/**
 * Created by PhpStorm.
 * User: long
 * Date: 2016/9/25
 * Time: 14:04
 */

namespace Notify\Controller;


use Common\Model\MemberModel;
use Common\Model\OrderModel;
use Think\Controller;

/**
 * 回调订单处理业务逻辑
 * Class NotifybaseController
 * @package Notify\Controller
 */
class NotifybaseController extends Controller
{
    public $order_model;
    public $member_model;

    public function __construct()
    {
        parent::__construct();
        $this->order_model = new OrderModel();
        $this->member_model = new MemberModel();
//        $this->product_option_model = new ProductOptionModel();
    }




}