<?php
/**
 * Created by PhpStorm.
 * User: long
 * Date: 2016/9/25
 * Time: 14:05
 */

namespace Notify\Controller;

use Common\Model\LessonModel;
use Common\Model\LessonOrderModel;
use Common\Model\MemberModel;
use Common\Model\OrderModel;
use Common\Model\ProductModel;
use Common\Model\SystemMsgModel;
use System\Model\SmsModel;
use Think\Log;


/**
 * 支付宝
 * Class AlipayController
 * @package Notify\Controller
 */
class AlipayController extends NotifybaseController
{
    public $order_model;
    public $smslog_model;
    public $system_msg_model;

    public $member_model;
    public $lesson_order_model;
    public $product_model;
    public $lesson_model;



    public function __construct()
    {
        parent::__construct();
        $this->order_model          = new OrderModel();
        $this->smslog_model         = new SmsModel();
        $this->system_msg_model     = new SystemMsgModel();

        $this->member_model         = new MemberModel();
        $this->lesson_order_model   = new LessonOrderModel();
        $this->product_model        = new ProductModel();
        $this->lesson_model         = new LessonModel();

        vendor('Alipay.RSAfunction');
        vendor('Alipay.Corefunction');
        vendor('Alipay.Md5function');
        vendor('Alipay.Notify');
        vendor('Alipay.Submit');
    }

//    "discount":0.00
//    "payment_type":1
//    "subject":驰锐科技
//    "trade_no":2017040521001004350238888318
//    "buyer_email":1004377199@qq.com
//    "gmt_create":2017-04-05 11:17:24
//    "notify_type":trade_status_sync
//    "quantity":1
//    "out_trade_no":D2017040509499
//    "seller_id":2088621554896422
//    "notify_time":2017-04-05 11:17:24
//    "body":驰锐科技
//    "trade_status":TRADE_SUCCESS
//    "is_total_fee_adjust":Y
//    "total_fee":0.01
//    "seller_email":1970981039@qq.com
//    "price":0.01
//    "buyer_id":2088802995358355
//    "notify_id":58b3fa5374154820c41d8078b69fa77ipa
//    "use_coupon":N
//    "sign_type":RSA
//    "sign":PVi8ZGDswvifveLPoNtUgGDPt31k6Y33SH3/TRny/LtJEgNRSY/BSSwTvba5l2xlrFSlBEspNdN2POlwqJCdbeU/oSLfzNP/s+k8uN4n4Q2RVP7DL5/YRtLD4KthQtOYfiDW7NTFL5qynysMCmT8HhcalYjTsQvlXJOgOYmRUDE=
    public function index()
    {
//        Log::record(json_encode( IS_POST ? $_POST : $_GET), Log::WARN);
//        exit;
        $alipay_config = C('ALIPAY_CONFIG');
        $alipayNotify = new \AlipayNotify($alipay_config);
        $verify = $alipayNotify->verifyNotify();

        if (!$verify) {
            Log::record('支付宝回调校验失败1' . json_encode($alipay_config), Log::WARN);
            Log::record('支付宝回调校验失败2' . json_encode(I('')), Log::WARN);
            Log::record(json_encode('失败'), Log::WARN);
            echo 'fail';
            exit;
        }


        if ($_POST['trade_status'] == 'TRADE_FINISHED' || $_POST['trade_status'] == 'TRADE_SUCCESS') {
            $data = I('post.');
            Log::record(json_encode('成功'), Log::WARN);
            if (empty($data))
                exit('empty');

            $order_sn = $data['out_trade_no'];
            $order_price = $data['total_fee'];

            $order = $this->order_model->where(['order_sn' => $order_sn])->find();
            if (!$order) {
                Log::record($order_sn . '订单编号不存在3', Log::WARN);
                exit('fail');
            }

            $data = [
                'status'   => OrderModel::STATUS_PAY_SUCCESS,
                'pay_type' => OrderModel::PAY_TYPE_ALIPAY,
                'pay_time' => date('Y-m-d H:i:s'),
                'back_data'     => json_encode($data),
                'update_time' => date('Y-m-d H:i:s'),
            ];

            $result = $this->order_model->where(['order_sn' => $order_sn])->save($data);
            $order_id = $this->order_model->where(['order_sn' => $order_sn])->getField('id');
            $this->product_model->changeStock($order_id);
            $msg = $this->system_msg_model->addOne($order['mid'], '您的订单已支付成功，请耐心等待发货');


            echo 'success';
        }
    }

    public function lesson() {
        //        Log::record(json_encode( IS_POST ? $_POST : $_GET), Log::WARN);
//        exit;
        $alipay_config = C('ALIPAY_CONFIG');
        $alipayNotify = new \AlipayNotify($alipay_config);
        $verify = $alipayNotify->verifyNotify();

        if (!$verify) {
            Log::record('支付宝回调校验失败1' . json_encode($alipay_config), Log::WARN);
            Log::record('支付宝回调校验失败2' . json_encode(I('')), Log::WARN);
            Log::record(json_encode('失败'), Log::WARN);
            echo 'fail';
            exit;
        }


        if ($_POST['trade_status'] == 'TRADE_FINISHED' || $_POST['trade_status'] == 'TRADE_SUCCESS') {
            $data = I('post.');
            Log::record(json_encode('成功'), Log::WARN);
            if (empty($data))
                exit('empty');

            $order_sn = $data['out_trade_no'];
            $order_price = $data['total_fee'];

            $order = $this->lesson_order_model->where(['order_sn' => $order_sn])->find();
            if (!$order) {
                Log::record($order_sn . '订单编号不存在3', Log::WARN);
                exit('fail');
            }

            $data = [
                'status'   => LessonOrderModel::STATUS_PAY_SUCCESS,
                'pay_type' => LessonOrderModel::PAY_TYPE_ALIPAY,
                'pay_time' => date('Y-m-d H:i:s'),
                'notify_message'    => json_encode($data),
                'update_time' => date('Y-m-d H:i:s'),
            ];

            $result = $this->lesson_order_model->where(['order_sn' => $order_sn])->save($data);
            $this->lesson_model->changeStock($order_sn);
            $msg = $this->system_msg_model->addOne($order['mid'], '您购买的课程已支付成功');


            echo 'success';
        }
    }
}