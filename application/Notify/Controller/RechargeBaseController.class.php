<?php
/**
 * Created by PhpStorm.
 * User: long
 * Date: 2016/8/2
 * Time: 15:43
 */

namespace Notify\Controller;

use Common\Model\HelpMemberModel;
use Common\Model\RechargeHealthModel;
use Common\Model\RechargeModel;
use Common\Model\ServiceWalletModel;
use Common\Model\WalletBillModel;
use Common\Model\WalletModel;
use Think\Controller;

use Think\Log;

/**
 * 充值基类
 * Class RechargeBaseController
 * @package Notify\Controller
 */
class RechargeBaseController extends Controller
{
    protected $wallet_model;
    protected $wallet_bill_model;
    protected $recharge_model;
    private   $_recharge_data;
    protected $service_wallet_model;
    protected $recharge_health_model;
    protected $help_member_model;

    public function __construct()
    {
        parent::__construct();
        $this->wallet_bill_model    = new WalletBillModel();
        $this->wallet_model         = new WalletModel();
        $this->recharge_model       = new RechargeModel();
        $this->service_wallet_model = new ServiceWalletModel();
        $this->recharge_health_model= new RechargeHealthModel();
        $this->help_member_model    = new HelpMemberModel();

    }

    /**
     * @param $out_trade_no
     * @param $price
     *
     * @return bool
     */
    public function chechValidity($out_trade_no, $price)
    {
        $this->_recharge_data = $this->recharge_model->where(['out_trade_no' => $out_trade_no])->find();
        if (!$this->_recharge_data)
            return false;

        /*if ($this->_recharge_data['total_fee'] != $price) {
            Log::record('充值金额与订单金额不符,订单编号为:' . $out_trade_no, Log::WARN);
            return false;
        }*/

        if ($this->_recharge_data['status'] == RechargeModel::STATUS_PAY_SUCCESS)
            return false;
        return true;
    }


    /**
     * 钱包金额增加
     * @return bool
     */
    public function compute($payType, $notify_message)
    {
        if (!$this->_recharge_data) {
            return false;
        }
        $pay_money = $this->_recharge_data['total_fee'];
        if ($pay_money <= 0) {
            return false;
        }

        $iscommit = true;
        $this->recharge_model->startTrans();
        $mid = $this->_recharge_data['mid'];
        if($this->_recharge_data['type'] == RechargeModel::TYPE_RECHARGE) {
            $source = WalletBillModel::SOURCE_RECHARGE;
            $before_change = $this->wallet_model->getBalance($mid);
            $result_wallet = $this->wallet_model->addmoney($mid, $pay_money);
            if ($result_wallet === false) {
                $iscommit = false;
            }
        } else if($this->_recharge_data['type'] == RechargeModel::TYPE_SERVICE) {
            $source = WalletBillModel::SOURCE_SERVICE;
            $before_change = $this->service_wallet_model->where(['mid' => $mid])->getField('balance');
            if($this->service_wallet_model->addMoney($mid, $pay_money) == false) {
                $iscommit = false;
            }
        }


        //钱包流水记录
        $result_wallet_bill = $this->wallet_bill_model->addBill($mid, $pay_money, $before_change, $source, WalletBillModel::BILL_TYPE_IN);

        if ($result_wallet_bill == false) {
            $iscommit = false;
        }

        $result_change_status = $this->recharge_model
            ->where(['out_trade_no' => $this->_recharge_data['out_trade_no']])
            ->save([
                'notify_status'  => RechargeModel::NOTIFY_STATUS_SUCCESS,
                'notify_time'    => getDefaultTime(),
                'update_time'    => getDefaultTime(),
                'status'         => RechargeModel::STATUS_PAY_SUCCESS,
                'paytype'        => $payType,
                'notify_message' => $notify_message,
            ]);


        if ($result_change_status === false)
            $iscommit = false;

        if ($iscommit) {
            $this->recharge_model->commit();
            return true;
        } else {
            $this->recharge_model->rollback();
            return false;
        }
    }

    /**
     * @param $out_trade_no
     * @param $price
     * @return bool
     */
    public function chechValidityHealth($out_trade_no, $price)
    {
        $this->_recharge_data = $this->recharge_health_model->where(['out_trade_no' => $out_trade_no])->find();
        if (!$this->_recharge_data)
            return false;

        /*if ($this->_recharge_data['total_fee'] != $price) {
            Log::record('充值金额与订单金额不符,订单编号为:' . $out_trade_no, Log::WARN);
            return false;
        }*/

        if ($this->_recharge_data['status'] == RechargeModel::STATUS_PAY_SUCCESS)
            return false;
        return true;
    }

    /**
     * @param $payType
     * @param $notify_message
     * @return bool
     * 充值健康值回调处理
     */
    public function dealHealth($payType, $notify_message)
    {
        if (!$this->_recharge_data) {
            return false;
        }
        $pay_money = $this->_recharge_data['total_fee'];
        if ($pay_money <= 0) {
            return false;
        }

        $iscommit = true;
        $this->recharge_model->startTrans();
        $mid = $this->_recharge_data['mid'];
        $h_id = $this->_recharge_data['h_id'];
        $h_info = $this->help_member_model->find($h_id);

        //钱包流水记录
        $result_wallet_bill = $this->wallet_bill_model->addBill($mid, $pay_money, $h_info['balance'], WalletBillModel::SOURCE_HEALTH, WalletBillModel::BILL_TYPE_IN);

        if($h_info['balance']+$pay_money >= 10) {
            $data = [
                'balance'   => $h_info['balance']+$pay_money,
                'if_need'   => 'no',
                'update_time'   => getDefaultTime(),
            ];
        } else {
            $data = [
                'balance'   => $h_info['balance']+$pay_money,
                'update_time'   => getDefaultTime(),
            ];
        }
        if($this->help_member_model->where(['id' => $h_id])->save($data) == false) {
            $iscommit = false;
        }

        if ($result_wallet_bill == false) {
            $iscommit = false;
        }

        $result_change_status = $this->recharge_model
            ->where(['out_trade_no' => $this->_recharge_data['out_trade_no']])
            ->save([
                'notify_status' => RechargeModel::NOTIFY_STATUS_SUCCESS,
                'notify_time'   => getDefaultTime(),
                'update_time'   => getDefaultTime(),
                'status'        => RechargeModel::STATUS_PAY_SUCCESS,
                'paytype'       => $payType,
                'notify_message'=> $notify_message,
            ]);


        if ($result_change_status === false)
            $iscommit = false;

        if ($iscommit) {
            $this->recharge_model->commit();
            return true;
        } else {
            $this->recharge_model->rollback();
            return false;
        }
    }

}