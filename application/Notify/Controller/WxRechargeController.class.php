<?php
/**
 * Created by PhpStorm.
 * User: long
 * Date: 2016/8/2
 * Time: 15:44
 */

namespace Notify\Controller;

use Common\Model\RechargeModel;
use Think\Log;

/**
 * 微信充值回调
 * Class WxRechargeController
 * @package Notify\Controller
 */
class WxRechargeController extends RechargeBaseController
{

    public function __construct()
    {
        parent::__construct();
        header("Content-type:text/html;charset=utf-8");
        ini_set('date.timezone', 'Asia/Shanghai');
        vendor('WxPayPubHelper.WxPayPubHelper');
    }

    public function index()
    {
        $notify = new \Notify_pub();
        $xml = $GLOBALS['HTTP_RAW_POST_DATA'];
        if (!$xml) return false;
        $notify->saveData($xml);


        //签名状态
        $checkSign = true;
        if ($notify->checkSign() == FALSE) {
            $notify->setReturnParameter("return_code", "FAIL");//返回状态码
            $notify->setReturnParameter("return_msg", "签名失败");//返回信息
            $checkSign = false;
        } else {
            $notify->setReturnParameter("return_code", "SUCCESS");//设置返回码
        }
        $returnXml = $notify->returnXml();

        echo $returnXml;
        if (!$checkSign) exit;

        $back_data = $notify->getData();
        $out_trade_no = $back_data['out_trade_no']; //订单号
        $total_fee = $back_data['total_fee'] / 100; //微信返回的是分，换算成元




        if (!$this->chechValidity($out_trade_no, $total_fee)) {
            Log::record(json_encode($back_data), Log::WARN);
            exit;
        }

        if (!$this->compute(RechargeModel::PAY_TYPE_WX, json_encode($back_data))) {
            Log::record(json_encode($back_data), Log::WARN);
            exit;
        }
    }

    /**
     * 健康值充值回调
     */
    public function health() {
        $notify = new \Notify_pub();
        $xml = $GLOBALS['HTTP_RAW_POST_DATA'];
        if (!$xml) return false;
        $notify->saveData($xml);


        //签名状态
        $checkSign = true;
        if ($notify->checkSign() == FALSE) {
            $notify->setReturnParameter("return_code", "FAIL");//返回状态码
            $notify->setReturnParameter("return_msg", "签名失败");//返回信息
            $checkSign = false;
        } else {
            $notify->setReturnParameter("return_code", "SUCCESS");//设置返回码
        }
        $returnXml = $notify->returnXml();

        echo $returnXml;
        if (!$checkSign) exit;

        $back_data = $notify->getData();
        $out_trade_no = $back_data['out_trade_no']; //订单号
        $total_fee = $back_data['total_fee'] / 100; //微信返回的是分，换算成元




        if (!$this->chechValidityHealth($out_trade_no, $total_fee)) {
            Log::record(json_encode($back_data), Log::WARN);
            exit;
        }

        if (!$this->dealHealth(RechargeModel::PAY_TYPE_WX, json_encode($back_data))) {
            Log::record(json_encode($back_data), Log::WARN);
            exit;
        }
    }

}