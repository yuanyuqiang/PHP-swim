<?php
/**
 * Created by PhpStorm.
 * User: yunlongw
 * Date: 2017/2/28
 * Time: 下午3:03
 */

namespace AppApi\Controller;


use Think\Cache\Driver\Redis;

class TestController extends ApiBaseController
{
    public function index()
    {
        $redis = new Redis();
        $redis->connect('localhost', 6379);
        $redis->set('test', 'hello world!');
        echo $redis->get("test");

        dump();
    }
}