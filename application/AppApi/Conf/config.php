<?php
return [
    //'配置项'=>'配置值'

    //阿里支付
    'ALIPAY_CONFIG' => [
        'partner'               => '',
        'seller_id'             => '',
        'key'                   => '',
        'seller_email'          => '',
        'sign_type'             => strtolower('RSA'),
        'input_charset'         => strtolower('utf-8'),
        'cacert'                => '',
        'rsa_private_key'       => getcwd() . '/simplewind/Core/Library/Vendor/Alipay/lib/rsa_private_key.pem',
        'rsa_public_key'        => getcwd() . '/simplewind/Core/Library/Vendor/Alipay/lib/rsa_public_key.pem',
        'rsa_private_key_pkcs8' => getcwd() . '/simplewind/Core/Library/Vendor/Alipay/lib/rsa_private_key_pkcs8.pem',
        'transport'             => 'http',
        'notify_url'            => 'http://***/Notify/Alipay/index', //支付回调地址
        'notify_recharge_url'   => 'http://***/Notify/AlipayRecharge/index', //充值回调地址
        'return_url'            => 'm.alipay.com',
        'ali_public_key'        => '',
    ],

    //短信配置
    'SMS_ACCOUNT'   => [
        'userid'   => '',
        'account'  => '',
        'password' => '',
        'company'  => '',
    ],


    'DATA_CACHE_PREFIX' => 'Redis_',//缓存前缀
    'DATA_CACHE_TYPE'   => 'Redis',//默认动态缓存为Redis
    'REDIS_RW_SEPARATE' => true, //Redis读写分离 true 开启
    'REDIS_HOST'        => 'localhost', //redis服务器ip，多台用逗号隔开；读写分离开启时，第一台负责写，其它[随机]负责读；
    'REDIS_PORT'        => '6379',//端口号
    'REDIS_TIMEOUT'     => '300',//超时时间
    'REDIS_PERSISTENT'  => false,//是否长连接 false=短连接
    'REDIS_AUTH'        => '',//AUTH认证密码
];