<?php
/**
 * Created by PhpStorm.
 * User: yyq
 * Date: 2018/8/19
 * Time: 14:48
 */

namespace Order\Controller;


use Common\Controller\AdminbaseController;
use Common\Model\MemberModel;
use Common\Model\OrderModel;

class OrderController extends AdminbaseController
{
    private $member_model;
    private $order_model;

    public function __construct()
    {
        parent::__construct();
        $this->member_model = new MemberModel();
        $this->order_model  = new OrderModel();
    }


    public function lists() {
        $where['status'] = 'payed';

        $count = $this->order_model->where($where)->count();

        $page = $this->page($count, C("PAGE_NUMBER"));
        $result = $this->order_model
            ->where($where)
            ->limit($page->firstRow, $page->listRows)
            ->select();
        foreach ($result as $k => $v) {
            $result[$k]['card_info'] = json_decode($v['card_info'], true);
        }
        $para['list'] = $result;
        $para['Page'] = $page->show();
        $para['formget']    = I('');
        $this->assign($para);
        $this->display();
    }
}