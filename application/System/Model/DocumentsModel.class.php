<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2016/6/21
 * Time: 16:55
 */

namespace System\Model;


use Common\Model\CommonModel;

class DocumentsModel extends CommonModel
{
    //array(验证字段,验证规则,错误提示,验证条件,附加规则,验证时间)
    protected $_validate = [
        ['doc_class', 'require', '文档类型必填！', 1, 'regex', CommonModel:: MODEL_BOTH],
        ['name', 'require', '名称必选！', 1, 'regex', CommonModel:: MODEL_BOTH],
        ['desc', 'require', '描述必填！', 1, 'regex', CommonModel:: MODEL_BOTH],
        ['content', 'require', '内容必填！', 1, 'regex', CommonModel:: MODEL_BOTH],
    ];

    //自动完成
    protected $_auto = [
        ['create_time', 'autoDateTime', 1, 'callback'],
        ['update_time', 'autoDateTime', 3, 'callback'],
    ];

    function autoDateTime()
    {
        return date('Y-m-d H:i:s', time());
    }

    public function getDocument($name)
    {
        return $this->where(['doc_class' => $name])->find();
    }
}