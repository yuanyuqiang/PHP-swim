<?php
/**
 * Created by PhpStorm.
 * User: yunlongw
 * Date: 2016/12/23
 * Time: 上午10:03
 */

namespace System\Model;


use Common\Model\CommonModel;

class SmsModel extends CommonModel
{
    const TYPE_1 = 1;//注册
    const TYPE_2 = 2;//忘记密码
    const TYPE_3 = 3;//支付密码修改

    /**
     * @param $type
     * @return string
     * 类型转字符串
     */
    function typeToString($type) {
        switch ($type) {
            case self::TYPE_1:
                return '注册';
            break;
            case self::TYPE_2:
                return '忘记密码';
            break;
            case self::TYPE_3:
                return '修改支付密码';
            break;
            default:
                return '';
            break;
        }
    }
}