<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2016/6/21
 * Time: 16:55
 */

namespace System\Model;


use Common\Model\CommonModel;

class DocClassModel extends CommonModel
{
    protected $tableName = "doc_class";
    //自动验证
    protected $_validate = [
        //array(验证字段,验证规则,错误提示,验证条件,附加规则,验证时间)
        ['key_name', 'require', 'key_name键名必填！', 1, 'regex', CommonModel:: MODEL_BOTH],
        ['key_desc', 'require', 'key_desc描述必选！', 1, 'regex', CommonModel:: MODEL_BOTH],
    ];

    //自动完成
    protected $_auto = [
        ['create_time', 'autoDateTime', 1, 'callback'],
        ['update_time', 'autoDateTime', 3, 'callback'],
    ];

    function autoDateTime()
    {
        return date('Y-m-d H:i:s', time());
    }
}