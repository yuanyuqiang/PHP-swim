<?php
/**
 * Created by PhpStorm.
 * User: yunlongw
 * Date: 2016/12/22
 * Time: 上午11:29
 */

namespace System\Model;


use Common\Model\CommonModel;
use Think\Log;

class RequestRecordModel extends CommonModel
{
    //array(验证字段,验证规则,错误提示,验证条件,附加规则,验证时间)
    protected $_validate = [
        ['request_method', 'require', 'request_method is not null！', 1, 'regex', CommonModel:: MODEL_BOTH],
        ['controller_action', 'require', 'controller_function is not null！', 1, 'regex', CommonModel:: MODEL_BOTH],
        ['equipment', 'require', 'equipment is not null！', 1, 'regex', CommonModel:: MODEL_BOTH],
        ['ip', 'require', 'ip is not null！', 1, 'regex', CommonModel:: MODEL_BOTH],
        ['create_time', 'require', 'create_time is not null！', 1, 'regex', CommonModel:: MODEL_BOTH],
    ];


    /**
     * 新增记录
     *
     * @param $request_method
     * @param $controller_action
     * @param $equipment
     * @param $values
     * @param $ip
     *
     * @return string
     */
    public function addRecord($request_method, $controller_action, $equipment, $values, $ip)
    {
        return $this->add(
            [
                'request_method'    => $request_method,
                'controller_action' => $controller_action,
                'equipment'         => $equipment,
                'values'            => $values,
                'ip'                => $ip,
                'create_time'       => date('Y-m-d H:i:s', time()),
            ]
        );
    }


    /**
     * 获取请求类型下拉选项
     *
     * @param $status
     *
     * @return string
     */
    public function getControllerActionOption($status = '')
    {
        $controller_action = $this
            ->field('controller_action')
            ->group('controller_action')
            ->select();
        $controller_action_option = '';
        foreach ($controller_action as $k => $v) {
            $state = '';
            if ($status == $v['controller_action']) $state = 'selected="selected"';
            $controller_action_option .= '<option  ' . $state . ' name="' . $v['controller_action'] . '">' . $v['controller_action'] . '</option>';
        }

        return $controller_action_option;
    }


    /**
     * @param $str
     *
     * @return string
     */
    public function getRequestMethodOption($str)
    {
        $option = '';
        $option .= '<option ' . ($str == "GET" ? 'selected="selected"' : '') . ' value="GET">GET</option>';
        $option .= '<option ' . ($str == "POST" ? 'selected="selected"' : '') . ' value="POST">POST</option>';

        return $option;
    }


    /**
     * @param $str
     *
     * @return string
     */
    public function getEquipmentOption($str)
    {
        $result = $this->field('equipment')->group('equipment')->select();
        $option = '';

        foreach ($result as $k => $v) {
            $state = '';
            if ($str == $v) $state = 'selected = "selected"';
            $option .= '<option  ' . $state . '  value="' . $v['equipment'] . '">' . $v['equipment'] . '</option>';
        }

        return $option;
    }


    /**
     * 标记成功
     *
     * @param $id
     * @param $data
     * @param $code
     * @return bool
     */
    public function resultSuccess($id, $data, $code)
    {
        $data = $data ? json_encode($data) : json_encode(['msg' => '成功']);
        return $this->where(['id' => $id])->save(['result' => 1, 'success_record' => $data, 'code' => $code]);
    }

    /**
     * 失败提示语句
     *
     * @param $id
     * @param $record
     *
     * @return bool
     */
    public function recordLog($id, $code, $record = '')
    {
        return $this->where(['id' => $id])->save(['record' => $record, 'code' => $code]);
    }
}