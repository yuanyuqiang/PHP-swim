<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2016/7/9
 * Time: 16:09
 */

namespace System\Controller;


use System\Model\SmsModel;
use Common\Controller\AdminbaseController;


class MessageController extends AdminbaseController
{
    protected $sms_model;

    public function __construct()
    {
        parent::__construct();
        $this->sms_model = new SmsModel();
    }

    public function lists()
    {
        $this->_lists();
        $this->display();
    }

    private function _lists()
    {
        $keyword = I('keyword');
        if (!empty($keyword)) {
            $where['mobile'] = ['like', "%$keyword%"];
            $_GET['keyword'] = $keyword;
        }
        $count = $this->sms_model->where($where)->count();
        $page = $this->page($count, C("PAGE_NUMBER"));
        $result = $this->sms_model
            ->limit($page->firstRow . ',' . $page->listRows)
            ->where($where)
            ->order('id desc')
            ->select();

        $categorys = '';
        foreach ($result as $k => $v) {

            $categorys .= '<tr>
            <td>' . ($k + 1) . '</td>
            <td>' . $result[$k]['mobile'] . '</td>
            <td>' . $result[$k]['code'] . '</td>
            <td>' . $this->sms_model->typeToString($v['type']) . '</td>
            <td>' . $result[$k]['create_time'] . '</td>
            <td>' . $result[$k]['end_time'] . '</td>
            <td>' . (date('Y-m-d H:i:s') > $result[$k]['end_time'] ? '<span class="text-error">已过期</span>' : '<span class="text-success">未过期</span>') . '</td>
        </tr>';
        }

        $this->assign('formget', I(''));
        $this->assign('categorys', $categorys);
        $this->assign("Page", $page->show());
    }
}